import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Create an array of fruits
        String[] fruits = new String[5];
        fruits[0] = "apple";
        fruits[1] = "avocado";
        fruits[2] = "banana";
        fruits[3] = "kiwi";
        fruits[4] = "banana";

        System.out.println("Fruits in Stock: " + Arrays.toString(fruits));

        Scanner sc = new Scanner(System.in);
        System.out.println("Which fruit would you like to get the index of?");
        String fruit = sc.nextLine();
        int indexOf = Arrays.binarySearch(fruits, fruit);

        System.out.println("The index of " + fruit + " is: " + indexOf );

        //Create an array of friends
        ArrayList<String> friends = new ArrayList<>();

        friends.add("John");
        friends.add("Jane");
        friends.add("Chloe");
        friends.add("Zoey");

        System.out.println("My friends are: " + friends);

        //create a hashmap
        HashMap<String, Integer> inventory = new HashMap<>();

        inventory.put("toothpaste", 15);
        inventory.put("toothbrush", 20);
        inventory.put("soap", 12);

        System.out.println("Our inventory consists of: " + "\n" + inventory);









    }
}